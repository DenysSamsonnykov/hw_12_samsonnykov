<?php

class Apartment extends Product {
    public $kitchen;
    public function __construct($title, $type, $address, $price, $description, $kitchen) {
       parent::__construct($title, $type, $address, $price, $description);
        $this->kitchen = $kitchen;
    }

    public function getSummaryLine(){
        return parent:: getSummaryLine() .'  '. $this->kitchen;
    }
}


?>