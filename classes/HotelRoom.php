<?php

class HotelRoom extends Product {
    public $roomNumber;
    public function __construct($title, $type, $address, $price, $description, $roomNumber) {
       parent::__construct($title, $type, $address, $price, $description);
        $this->roomNumber = $roomNumber;
    }

    public function getSummaryLine(){
        return parent:: getSummaryLine() .'  '. $this->roomNumber;
    }
}

?>