<?php

class House extends Product {
    public $roomsAmount;
    public function __construct($title, $type, $address, $price, $description, $roomsAmount) {
       parent::__construct($title, $type, $address, $price, $description);
        $this->roomsAmount = $roomsAmount;
    }

    public function getSummaryLine(){
        return parent:: getSummaryLine() .'  '. $this->roomsAmount;
    }
}
?>