<?php

$products = [
    [
        'title' => 'Odessa',
        'type' => 'hotel_room',
        'address' => 'ул. Генуэзская 32',
        'price' => '750 грн',
        'description' => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.',
        'roomNumber' => '3'
    ],
    [
        'title' => 'Золотой берег',
        'type' => 'apartment',
        'address' => 'ул. Бабушкина 46',
        'price' => '4500 грн',
        'description' => 'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. ',
        'kitchen' => true
    ],
    [
        'title' => 'У Егора',
        'type' => 'house',
        'address' => 'ул. Солнечная 78',
        'price' => '2000 грн',
        'description' => ' Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum',
        'roomsAmount' => '3'
    ],
    [
        'title' => 'Юность',
        'type' => 'hotel_room',
        'address' => 'ул. Пионерская 3',
        'price' => '1100 грн',
        'description' => ' Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.',
        'roomNumber' => '302'
    ],
    [
        'title' => 'Дерибас',
        'type' => 'hotel_room',
        'address' => 'ул. Дерибасовская 2',
        'price' => '1500 грн',
        'description' => ' Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
        'roomNumber' => '205'
    ],
    [
        'title' => 'Зеленый Мыс',
        'type' => 'apartment',
        'address' => 'ул. Николаевское шоссе 7',
        'price' => '5000 грн',
        'description' => 'At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga.  ',
        'kitchen' => false
    ],
    [
        'title' => 'Рыбачья тайна',
        'type' => 'apartment',
        'address' => 'ул. Дача Ковалевского 169',
        'price' => '2300 грн',
        'description' => 'Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.',
        'kitchen' => true
    ],
    [
        'title' => 'Павильон комфорта',
        'type' => 'house',
        'address' => 'ул. Ветренная 45',
        'price' => '6700 грн',
        'description' => 'Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae.',
        'roomsAmount' => '5'
    ],
    [
        'title' => 'Дом отдыха',
        'type' => 'house',
        'address' => 'ул. Абрикосовая 38',
        'price' => '3250',
        'description' => ' Et harum quidem rerum facilis est et expedita distinctio. ',
        'roomsAmount' => '2'
    ],
    [
        'title' => 'Гостевой домик',
        'type' => 'house',
        'address' => 'ул. Сортировочная 10',
        'price' => '900 грн',
        'description' => 'Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.',
        'roomsAmount' => '1'
    ]
];

?>