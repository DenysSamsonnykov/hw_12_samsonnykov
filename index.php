<?php
require_once $_SERVER['DOCUMENT_ROOT'] . '/data/products.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Product.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/House.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/HotelRoom.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/classes/Apartment.php';
require_once $_SERVER['DOCUMENT_ROOT'] . '/data/objects.php';


//echo '<pre>';
//var_dump($objects);


?>


<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="Description" content="OOP is awesome if you now what I mean!">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-eOJMYsd53ii+scO/bJGFsiCZc+5NDVN2yr8+0RDqr0Ql0h+rP48ckxlpbzKgwra6" crossorigin="anonymous">
    <link href="style.css" rel="stylesheet">
    <title>Start OOP</title>
</head>
<body>
    <h1 class="text-center">Предлагаемая в аренду недвижимость:</h1>
    <?php foreach($objects as $object): ?>
        <div class="card col-2">
            <div class="card-body">
                <h5 class="card-title"><?=$object->title?></h5>
                <p class="card-text"><?=$object->type;?></p>
                <p class="card-text"><?=$object->price?></p>
                <a href="writer.php?title=<?=$object->title; ?>" class="btn btn-warning">Подробнее</a>
            </div>
        </div>
    <?php endforeach; ?>
    <?=$object->getSummaryLine(); ?><!-- вывожу просто для примера, что она работает, в условии было написано, что у каждого обьекта должен быть доступен, но за её использование ни слова. Решил её не трогать -->

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/js/bootstrap.bundle.min.js" integrity="sha384-JEW9xMcG8R+pH31jmWH6WWP0WintQrMb4s7ZOdauHnUtxwoG2vI5DkLtS3qm9Ekf" crossorigin="anonymous"></script>
</body>
</html>